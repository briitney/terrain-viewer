uniform int isWater;
void main() {
    vec3 base_rgb;
    if (isWater == 1) {
        base_rgb = vec3(56.0f, 125.0f, 128.0f);
    } else {
        base_rgb = vec3(96.0f, 128.0f, 56.0f);
    }
    vec3 normalized_rgb = base_rgb/255.0f;
    gl_FragColor = vec4(normalized_rgb, 1.0f);;
}

#include <iostream>
#include <cmath>

double ** getRandomData(int w, int h, int mySeed) {
    double ** dumbData = new double*[w];
    for (int i = 0; i < w; i++) {
        srand(mySeed+i);
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = rand()%100/100.0;
        }
    }
    return dumbData;
}

/* Perlin Noise Algorithm taken right from the horse's mouth
 * at http://mrl.nyu.edu/~perlin/noise/, © Ken Perlin.
 * Slightly altered to C++ code.
 */

double fade(double t) {
    return t*t*t*(t*(t*6.0 - 15.0) + 10.0);
}

double myFade(double t) {
    return std::pow((t-0.5)*1.587,3) + 0.5;
}

double** getRandomWithFade(int w, int h, int mySeed) {
    double ** dumbData = new double*[w];
    for (int i = 0; i < w; i++) {
        srand(mySeed+i);
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            if (j < h/2) {
                dumbData[i][j] = myFade((rand()%100)/100.0);
            } else {
                dumbData[i][j] = fade((rand()%100)/100.0);
            }
        }
    }
    return dumbData;
}

double lerp(double t, double a, double b) {
    return a + t * (b - a);
}

double grad(int hash, double x, double y, double z) {
   int h = hash & 15;
   double u = h<8 ? x : y,
          v = h<4 ? y : h==12||h==14 ? x : z;
   return ((h&1) == 0 ? u : -u) + ((h&2) == 0 ? v : -v);
}

int* repeatRandom256(int mySeed) {
    int* data = new int[512];
    srand(mySeed);
    for (int i = 0; i < 256; i++) {
        data[i] = i;
    }
    for (int i = 0; i < 256; i++) {
        int j = rand()%(256-i)+i;
        double temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }
    for (int i = 256; i < 512; i++) {
        data[i] = data[i-256];
    }
    return data;
}

double perlinAt(double x, double y, int* p) {
    int z = 0;

    int X = (int)std::floor(x) & 255;
    int Y = (int)std::floor(y) & 255;
    int Z = (int)std::floor(z) & 255;

    x -= std::floor(x);
    y -= std::floor(y);
    z -= std::floor(z);

    double u = fade(x);
    double v = fade(y);
    double w = fade(z);

    int A = p[X]+Y;
    int AA = p[A]+Z;
    int AB = p[A+1]+Z;
    int B = p[X+1]+Y;
    int BA = p[B]+Z;
    int BB = p[B+1]+Z;

    return lerp(w, lerp(v, lerp(u, grad(p[AA  ], x  , y  , z   ),
                                   grad(p[BA  ], x-1, y  , z   )),
                           lerp(u, grad(p[AB  ], x  , y-1, z   ),
                                   grad(p[BB  ], x-1, y-1, z   ))),
                   lerp(v, lerp(u, grad(p[AA+1], x  , y  , z-1 ),
                                   grad(p[BA+1], x-1, y  , z-1 )),
                           lerp(u, grad(p[AB+1], x  , y-1, z-1 ),
                                   grad(p[BB+1], x-1, y-1, z-1 ))));
}

double ** perlinNoise(int w, int h, int mySeed) {
    int * array = repeatRandom256(mySeed);
    double ** dumbData = new double*[w];
    double min, max;
    min = max = 0;
    for (int i = 0; i < w; i++) {
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = perlinAt(i/10.0, j/10.0, array)/2+0.5;
            if (dumbData[i][j] > max) {
                max = dumbData[i][j];
            } else if (dumbData[i][j] < min) {
                min = dumbData[i][j];
            }
        }
    }
    double heightAdj = (max + min)/2;
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = (dumbData[i][j]-heightAdj)/((max-min))+0.5;
        }
    }
    delete[] array;
    return dumbData;
}

double ** layeredPerlinNoise(int w, int h, int mySeed) {
    int * array = repeatRandom256(mySeed);
    double ** dumbData = new double*[w];
    double min, max;
    min = max = 0;
    for (int i = 0; i < w; i++) {
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            double tote = 0;
            tote += perlinAt(i/10.0, j/10.0, array)/2+0.5;
            tote += tote;
            tote += perlinAt(i/20.0, j/20.0, array)/2+0.5;
            tote += perlinAt(i/5.0, j/5.0, array)/2+0.5;
            dumbData[i][j] = tote/4;
            if (dumbData[i][j] > max) {
                max = dumbData[i][j];
            } else if (dumbData[i][j] < min) {
                min = dumbData[i][j];
            }
        }
    }
    double heightAdj = (max + min)/2;
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = (dumbData[i][j]-heightAdj)/((max-min))+0.5;
        }
    }
    delete[] array;
    return dumbData;
}

/* General idea gleaned from https://thebookofshaders.com/12, but adapted from glsl to cpp
    and using a different method of generating random points.
*/
double worleyAt(double x, double y, double** randx, double** randy) {
    int cx = std::floor(x/10.0)+1;
    int cy = std::floor(y/10.0)+1;

    double ax = x - (((int)x)/10)*10;
    double ay = y - (((int)y)/10)*10;
    double curr_min = 20;
    for (int i = -1; i < 2; i++) {
        for (int j = -1; j < 2; j++) {
            int eval_x = cx + i;
            int eval_y = cy + j;

            double n_x = randx[eval_x][eval_y]*10+10*i;
            double n_y = randy[eval_x][eval_y]*10+10*j;

            double dist = std::abs(std::sqrt((ax-n_x)*(ax-n_x)+(ay-n_y)*(ay-n_y)));
            if (dist < 1.5) {dist = 10;}
            if (dist < curr_min) curr_min = dist;
        }
    }
    return curr_min/10;
}

double ** worleyNoise(int w, int h, int mySeed) {
    double ** dumbData = new double*[w];
    int cwd10 = std::ceil(w/10.0);
    int chd10 = std::ceil(h/10.0);
    srand(mySeed);
    double ** randx = getRandomData(cwd10+2, chd10+2, rand());
    double ** randy = getRandomData(chd10+2, chd10+2, rand());
    double min, max;
    min = max = 0;
    for (int i = 0; i < w; i++) {
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = worleyAt(i, j, randx, randy);
            if (dumbData[i][j] > max) {
                max = dumbData[i][j];
            } else if (dumbData[i][j] < min) {
                min = dumbData[i][j];
            }
        }
    }
    double heightAdj = (max + min)/2;
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = (dumbData[i][j]-heightAdj)/((max-min))+0.5;
        }
    }
    delete[] randx;
    delete[] randy;
    return dumbData;
}

double ** layeredWorleyNoise(int w, int h, int mySeed) {
    srand(mySeed);
    double ** dumbData = new double*[w];
    int cwd10 = std::ceil(w/10.0);
    int chd10 = std::ceil(h/10.0);
    double ** randx = getRandomData(cwd10+2, chd10+2, rand());
    double ** randy = getRandomData(chd10+2, chd10+2, rand());
    double min, max;
    min = max = 0;
    for (int i = 0; i < w; i++) {
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            double tote = 0;
            tote += worleyAt(i/2.5, j/2.5, randx, randy);
            tote += tote;
            tote += worleyAt(i, j, randx, randy);
            dumbData[i][j] = tote;
            if (dumbData[i][j] > max) {
                max = dumbData[i][j];
            } else if (dumbData[i][j] < min) {
                min = dumbData[i][j];
            }
        }
    }
    double heightAdj = (max + min)/2;
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = (dumbData[i][j]-heightAdj)/((max-min))+0.5;
        }
    }
    delete[] randx;
    delete[] randy;
    return dumbData;
}

/* Algorithm from http://c-faq.com/lib/gaussian.html */
/* Logic from Knuth, TAOCP vol 2, page 122 */
float gaussrand() {
    static double V1, V2, S;
    static int phase = 0;
    double X;
    if(phase == 0) {
        do {
            double U1 = (double)rand() / RAND_MAX;
            double U2 = (double)rand() / RAND_MAX;

            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
            } while(S >= 1 || S == 0);

        X = V1 * sqrt(-2 * log(S) / S);
    } else
        X = V2 * sqrt(-2 * log(S) / S);
    phase = 1 - phase;
    return X;
}

double ** gaussian (int w, int h, int mySeed) {
    double ** dumbData = new double*[w];
    double min, max;
    min = max = 0;
    for (int i = 0; i < w; i++) {
        srand(mySeed+i);
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = gaussrand();
            if (dumbData[i][j] > max) {
                max = dumbData[i][j];
            } else if (dumbData[i][j] < min) {
                min = dumbData[i][j];
            }
        }
    }
    double heightAdj = (max + min)/2;
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = (dumbData[i][j]-heightAdj)/((max-min))+0.5;
        }
    }
    return dumbData;
}

double ** waffle(int w, int h) {
    double ** dumbData = new double*[w];
    for (int i = 0; i < w; i++) {
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            if (i%10 == 0 || i%10 == 1) {
                dumbData[i][j] = (i*1.0/w)/2+0.25;
            } else if (i%10 == 5 || i%10 == 6) {
                dumbData[i][j] = (j*1.0/h)/2+0.25;
            } else {
                if (j%10 == 0 || j%10 == 1) {
                    dumbData[i][j] = (j*1.0/h)/2+0.25;
                } else if (j%10 == 5 || j%10 == 6) {
                    dumbData[i][j] = (i*1.0/w)/2+0.25;
                } else {
                    dumbData[i][j] = 0.5;
                }
            }
        }
    }
    return dumbData;
}

double** plain(int w, int h) {
    double ** dumbData = new double*[w];
    for (int i = 0; i < w; i++) {
        dumbData[i] = new double[h];
        for (int j = 0; j < h; j++) {
            dumbData[i][j] = (i+j)/((w+h)/2.0)/2.0;
        }
    }
    return dumbData;
}

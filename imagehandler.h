#ifndef IMAGEHANDLER_H
#define IMAGEHANDLER_H
#include <QImage>
#include <string>
#include <iostream>
#include "time.h"


class imageHandler
{
public:
    imageHandler(int w, int h, double** data);
    imageHandler(std::string name);
    ~imageHandler();

    void setData(double** data);
    double** getData();
    int getw();
    int geth();
    std::string getName();
    void setName(std::string name);
    bool save();
    bool save(std::string name);

private:
    int myW;
    int myH;
    double ** myData;
    std::string myName;
};

#endif // IMAGEHANDLER_H

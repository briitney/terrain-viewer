attribute vec3 posAttr;
attribute vec2 texAttr;
varying float col;
varying vec2 tex;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform int isWater;
uniform float waterAdj;
void main() {
    col = posAttr.z;
    tex = texAttr;
    if (isWater == 1) {
        float newz = posAttr.z*0.125+waterAdj;
        gl_Position = projection * view * model * vec4(posAttr.x, posAttr.y, newz, 1.0f);
    } else {
        gl_Position = projection * view * model * vec4(posAttr, 1.0f);
    }
}

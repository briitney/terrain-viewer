#include "datahandler.h"

dataHandler::dataHandler(int w, int h, double** data)
{
    myW = w;
    myH = h;
    myData = data;
    myVerts = NULL;
    myIndices = NULL;
    grabbed_data = false;
}

dataHandler::~dataHandler()
{
    if (grabbed_data) {
        myData = NULL;
    } else {
        for (int i = 0; i < myW; i++) {
            delete[] myData[i];
        }
        delete[] myData;
    }
    delete[] myVerts;
    delete[] myIndices;
}

double ** dataHandler::getData() {
    grabbed_data = true;
    return myData;
}

double* dataHandler::getVerts() {
    if (myVerts == NULL) dataToVerts();
    return myVerts;
}

unsigned int* dataHandler::getIndices() {
    if (myIndices == NULL) indicesForGrid();
    return myIndices;
}

int dataHandler::getw() {
    return myW;
}

int dataHandler::geth() {
    return myH;
}

int dataHandler::getVertsSize() {
    return myW*myH;
}
int dataHandler::getIndicesSize() {
    return 6*(myW-1)*(myH-1);
}

void dataHandler::dataToVerts() {
    myVerts = new double[myW*myH*5];
    for (int i = 0; i < myW; i++) {
        for (int j = 0; j < myH; j++) {
            myVerts[i*myH*5+j*5] = i;
            myVerts[i*myH*5+j*5+1] = j;
            myVerts[i*myH*5+j*5+2] = myData[i][j];
            myVerts[i*myH*5+j*5+3] = ((1-(float)i)/myW)*3;
            myVerts[i*myH*5+j*5+4] = ((1-((float)j))/myH)*3;
        }
    }
}

void dataHandler::indicesForGrid() {
    myIndices = new unsigned int[6*(myW-1)*(myH-1)];
    for (int i = 0, k = 0; i < (myW-1)*(myH-1); i++, k++) {
        if (k%myH == myH-1) k++;
        myIndices[i*6] = k+0;
        myIndices[i*6+1] = k+myH;
        myIndices[i*6+2] = k+1;
        myIndices[i*6+3] = k+myH;
        myIndices[i*6+4] = k+myH+1;
        myIndices[i*6+5] = k+1;
    }
}


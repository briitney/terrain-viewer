#include "myglwidget.h"
#include "algorithms.cpp"

MyGLWidget::MyGLWidget(QWidget *parent) : QOpenGLWidget(parent)
{
    notWireframe = true;
    currentTex = 0;
    heightAdj = 0;
    water = 0;
    waterAdj = 0.5;
    waterEnabled = 0;
    algo = 0;
    srand(time(NULL));
    verts = 100;

    xRot = -45;
    yRot = 0;
    zRot = -10;
    view.setToIdentity();
    view.rotate(xRot, 1.0, 0.0, 0.0);
    view.rotate(yRot, 0.0, 1.0, 0.0);
    view.rotate(zRot, 0.0, 0.0, 1.0);
    view.translate(0,0,0.125);
    data = NULL;
    blueShader = NULL;
    redShader = NULL;

    zoomAdj = 47.0;
}

MyGLWidget::~MyGLWidget()
{
    makeCurrent();
    delete program;
    delete data;
    delete wdata;
    delete redShader;
    delete blueShader;
    delete VAO;
    delete VBO;
    delete EBO;
    delete wVAO;
    delete wVBO;
    delete wEBO;
    doneCurrent();
}

void MyGLWidget::setColor()
{
    if (notWireframe) {
        program->removeShader(redShader);
        if (blueShader == NULL) {
            blueShader = new QOpenGLShader(QOpenGLShader::Fragment);
            blueShader->compileSourceFile("../terrain-viewer/bluefragmentshader.glsl");
        }
        program->addShader(blueShader);
    } else {
        program->removeShader(blueShader);
        program->addShader(redShader);
    }
    program->link();

    program->bind();
    program->setUniformValue(viewLoc, view);
    program->setUniformValue(modelLoc, model);
    program->setUniformValue(projectionLoc, projection);
    program->release();

    notWireframe = !notWireframe;
    update();
}

void MyGLWidget::setHeight(int val)
{
    heightAdj = val/200.0;
    if (data) {
        model.setToIdentity();
        model.scale(1.0/data->getw(), 1.0/data->geth(), -heightAdj);
        model.translate(-data->getw()/2, -data->geth()/2, -0.5);
        program->bind();
        program->setUniformValue(modelLoc, model);
        program->release();
    }
    update();
}

void MyGLWidget::setWaterHeight(int val)
{
    waterAdj = 1.0 - val/100.0;
    update();
}

void MyGLWidget::initializeGL()
{
    tex = new QOpenGLTexture(QImage("../terrain-viewer/textures/texture0.png").mirrored());
    tex->setWrapMode(QOpenGLTexture::Repeat);
    reseed();

    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);

    glClearColor(0.0f, 0.1f, 0.0f, 1.0f);

    program = new QOpenGLShaderProgram(this);

    redShader = new QOpenGLShader(QOpenGLShader::Fragment);
    redShader->compileSourceFile("../terrain-viewer/redfragmentshader.glsl");
    program->addShaderFromSourceFile(QOpenGLShader::Vertex, "../terrain-viewer/vertexshader.glsl");
    program->addShader(redShader);
    program->link();
    program->bind();

    posAttr = program->attributeLocation("posAttr");
    texAttr = program->attributeLocation("texAttr");
    viewLoc = program->uniformLocation("view");
    modelLoc = program->uniformLocation("model");
    projectionLoc = program->uniformLocation("projection");
    waterLoc = program->uniformLocation("isWater");
    waterAdjLoc = program->uniformLocation("waterAdj");

    program->setUniformValue(viewLoc, view);

    data = new dataHandler(100, 100, plain(100, 100));

    VBO = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    VBO->create();
    VBO->bind();

    VAO = new QOpenGLVertexArrayObject(this);
    VAO->create();
    VAO->bind();

    program->enableAttributeArray(posAttr);
    program->setAttributeBuffer(posAttr, GL_DOUBLE, 0, 3, 5*sizeof(double));
    program->enableAttributeArray(texAttr);
    program->setAttributeBuffer(texAttr, GL_DOUBLE, 3*sizeof(double), 2, 5*sizeof(double));

    EBO = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    EBO->create();

    VAO->release();
    VBO->release();

    wdata = new dataHandler(100, 100, layeredPerlinNoise(100, 100, mySeed));

    wVBO = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
    wVBO->create();
    wVBO->bind();

    wVAO = new QOpenGLVertexArrayObject(this);
    wVAO->create();
    wVAO->bind();

    program->enableAttributeArray(posAttr);
    program->setAttributeBuffer(posAttr, GL_DOUBLE, 0, 3, 5*sizeof(double));
    program->enableAttributeArray(texAttr);
    program->setAttributeBuffer(texAttr, GL_DOUBLE, 3*sizeof(double), 2, 5*sizeof(double));

    wEBO = new QOpenGLBuffer(QOpenGLBuffer::IndexBuffer);
    wEBO->create();

    wVAO->release();
    wVBO->release();

    program->release();

    loadDataIntoBuffers();
}

void MyGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    tex->bind();
    program->bind();
    if (!notWireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    VAO->bind();
    glDrawElements(GL_TRIANGLES, data->getIndicesSize(), GL_UNSIGNED_INT, 0);
    VAO->release();
    if (waterEnabled) {
        wVAO->bind();
        water = !water;
        program->setUniformValue(waterLoc, water);
        program->setUniformValue(waterAdjLoc, waterAdj);
        glDrawElements(GL_TRIANGLES, wdata->getIndicesSize(), GL_UNSIGNED_INT, 0);
        water = !water;
        program->setUniformValue(waterLoc, water);
        wVAO->release();
    }
    if (!notWireframe) glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    program->release();
    tex->release();
}

void MyGLWidget::resizeGL(int w, int h)
{
    projection.setToIdentity();
    projection.perspective(zoomAdj, w / double(h), 0.01f, 10.0f);
    projection.translate(0,0,-1.5);
    program->bind();
    program->setUniformValue(projectionLoc, projection);
    program->release();
}

void MyGLWidget::setZoom(int input) {
    zoomAdj = (100.0-input)/100.0*160+15;
    if (data) {
    resizeGL(this->width(), this->height());
    update();
    }
}

void MyGLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    if (event->buttons() & Qt::LeftButton) {
        setXRotation(xRot + dy);
        setZRotation(zRot + dx);
    } else if (event->buttons() & Qt::RightButton) {
        setYRotation(yRot + dx);
    }

    view.setToIdentity();
    view.rotate(xRot, 1.0, 0.0, 0.0);
    view.rotate(yRot, 0.0, 1.0, 0.0);
    view.rotate(zRot, 0.0, 0.0, 1.0);
    view.translate(0,0,0.125);
    program->bind();
    program->setUniformValue(viewLoc, view);
    program->release();
    lastPos = event->pos();
}

void MyGLWidget::setXRotation(int angle)
{
    if (angle != xRot) {
        xRot = angle%360;
        update();
    }
}

void MyGLWidget::setYRotation(int angle)
{
    if (angle != yRot) {
        yRot = angle%360;
        update();
    }
}

void MyGLWidget::setZRotation(int angle)
{
    if (angle != zRot) {
        zRot = angle%360;
        update();
    }
}

void MyGLWidget::saveToImage() {
    imageHandler img(data->getw(), data->geth(), data->getData());
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save file"), QString::fromStdString("../terrain-viewer/images/"+img.getName()+".png"));
    if (fileName.isEmpty()) {
        return;
    }
    if (!img.save(fileName.toStdString())) {
        QMessageBox::information(this, tr("Unable to save file"), "Could not save file to filesystem.");
    }
}

void MyGLWidget::loadFromImage() {
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "../terrain-viewer/images/");
    if (fileName.isEmpty()) {
        return;
    }
    try {
        imageHandler img(fileName.toStdString());
        delete data;
        data = new dataHandler(img.getw(), img.geth(), img.getData());
        wdata = new dataHandler(img.getw(), img.geth(), layeredPerlinNoise(img.getw(), img.geth(), mySeed));
        loadDataIntoBuffers();
        update();

    } catch (const std::runtime_error& e) {
        QMessageBox::information(this, tr("Unable to load file"), "Could not load file.");
    }
}


void MyGLWidget::loadDataIntoBuffers() {
    program->bind();

    model.setToIdentity();
    model.scale(1.0/data->getw(), 1.0/data->geth(), -heightAdj);
    model.translate(-data->getw()/2, -data->geth()/2, -0.5);
    program->setUniformValue(modelLoc, model);

    VAO->bind();
    VBO->bind();
    EBO->bind();

    VBO->allocate(data->getVerts(), sizeof(double)*5*data->getVertsSize());
    EBO->allocate(data->getIndices(), sizeof(unsigned int)*data->getIndicesSize());

    VAO->release();
    VBO->release();
    EBO->release();

    wVAO->bind();
    wVBO->bind();
    wEBO->bind();

    wVBO->allocate(wdata->getVerts(), sizeof(double)*5*wdata->getVertsSize());
    wEBO->allocate(wdata->getIndices(), sizeof(unsigned int)*wdata->getIndicesSize());

    wVAO->release();
    wVBO->release();
    wEBO->release();
    program->release();
}

void MyGLWidget::refresh () {
    reseed();
    regenData();
}

void MyGLWidget::reseed() {
    srand(time(NULL));
    mySeed = rand();
    emit seedUpdate(mySeed);
}

void MyGLWidget::setSeed(int val) {
    mySeed = val;
    regenData();
}

void MyGLWidget::regenData() {
    delete data;
    if (algo == 0) {
        data = new dataHandler(verts, verts, plain(verts, verts));
    } else if (algo == 1) {
        data = new dataHandler(verts, verts, waffle(verts, verts));
    } else if (algo == 2) {
        data = new dataHandler(verts, verts, getRandomData(verts, verts, mySeed));
    } else if (algo == 3) {
        data = new dataHandler(verts, verts, getRandomWithFade(verts, verts, mySeed));
    } else if (algo == 4) {
        data = new dataHandler(verts, verts, gaussian(verts, verts, mySeed));
    } else if (algo == 5) {
        data = new dataHandler(verts, verts, perlinNoise(verts, verts, mySeed));
    } else if (algo == 6) {
        data = new dataHandler(verts, verts, layeredPerlinNoise(verts, verts, mySeed));
    } else if (algo == 7) {
        data = new dataHandler(verts, verts, worleyNoise(verts, verts, mySeed));
    } else if (algo == 8) {
        data = new dataHandler(verts, verts, layeredWorleyNoise(verts, verts, mySeed));
    } else {
        /* I should never get here */
        exit(1);
    }
    wdata = new dataHandler(verts, verts, layeredPerlinNoise(verts, verts, mySeed));
    loadDataIntoBuffers();
    update();
}

void MyGLWidget::setAlgo(int index) {
    algo = index;
    regenData();
}

void MyGLWidget::setVertNum(int num) {
    verts = num;
    regenData();
}

void MyGLWidget::toggleWater() {
    waterEnabled = !waterEnabled;
    update();
}

void MyGLWidget::changeTex() {
    delete tex;
    currentTex++;
    QImage teximg = QImage(QString::fromStdString("../terrain-viewer/textures/texture"+std::to_string(currentTex)+".png")).mirrored();
    if (teximg.isNull()) {
        currentTex = 0;
        teximg = QImage(QString::fromStdString("../terrain-viewer/textures/texture"+std::to_string(currentTex)+".png")).mirrored();
    }
    tex = new QOpenGLTexture(teximg);
    tex->setWrapMode(QOpenGLTexture::Repeat);
    update();
}

#include "imagehandler.h"

imageHandler::imageHandler(int w, int h, double** data)
{
    srand(time(NULL));
    myName = "map_"+std::to_string(rand());
    myW = w;
    myH = h;
    myData = data;
}

imageHandler::imageHandler(std::string name) {
    myName = name;
    QImage image = QImage(QString::fromStdString(name), "PNG");
    if (image.isNull()) {
        throw std::runtime_error("Could not create file");
    }
    myW = image.width();
    myH = image.height();
    if (myW*myH > 500*500) {
        throw std::runtime_error("File too large");
    }
    myData = new double*[myW];
    for (int i = 0; i < myW; i++) {
        myData[i] = new double[myH];
    }
    for (int i = 0; i < myW; i++) {
        for (int j = 0; j < myH; j++) {
            myData[i][j] = 1-image.pixelColor(i, j).value()/255.0;
        }
    }
}

imageHandler::~imageHandler() {
    myData = NULL;
}

void imageHandler::setData(double** data) {
    myData = data;
}

double** imageHandler::getData() {
    return myData;
}

int imageHandler::geth() {
    return myH;
}

int imageHandler::getw() {
    return myW;
}

std::string imageHandler::getName() {
    return myName;
}

void imageHandler::setName(std::string name) {
    myName = name;
}

bool imageHandler::save() {
    return save("../terrain-viewer/images/"+myName+".png");
}

bool imageHandler::save(std::string name) {
    QImage image = QImage(myW, myH,  QImage::Format_Grayscale8);
    if (myData == NULL) return false;
    QRgb value;
    int data_val;
    for (int i = 0; i < myW; i++) {
        for (int j = 0; j < myH; j++) {
            data_val = 255-(myData[i][j]*255);
            value = qRgb(data_val, data_val, data_val);
            image.setPixel(i,j,value);
        }
    }
    return image.save(QString::fromStdString(name), "PNG", 100);
}

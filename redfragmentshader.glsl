uniform sampler2D texture;
varying vec2 tex;

varying float col;

uniform int isWater;

void main() {
    vec3 base_rgb;
    if (isWater == 1) {
        base_rgb = vec3(56.0f, 125.0f, 128.0f);
    } else {
        base_rgb = vec3(127.5f, 127.5f, 127.5f);
    }
    vec3 normalized_rgb = base_rgb/255.0f;
    if (isWater == 1) {
        gl_FragColor = vec4(normalized_rgb+normalized_rgb*(0.3f-col), 1.0f);
    } else {
        gl_FragColor = vec4(texture2D(texture, tex.st)+texture2D(texture, tex.st)*(0.5f-col)*1.5);
    }
}

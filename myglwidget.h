#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QObject>
#include <QWidget>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QTimer>
#include <QDir>
#include <QFileInfo>
#include <QMouseEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QOpenGLTexture>

#include <iostream>
#include "datahandler.h"
#include "imagehandler.h"

class MyGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    MyGLWidget(QWidget *parent);
    virtual ~MyGLWidget();
    void loadDataIntoBuffers();
    void regenData();
    void reseed();
    void setSeed(int val);

public slots:
    void setColor();
    void toggleWater();
    void setHeight(int val);
    void setWaterHeight(int val);
    void saveToImage();
    void loadFromImage();
    void setAlgo(int a);
    void setVertNum(int a);
    void refresh();
    void changeTex();
    void setZoom(int input);

signals:
    void seedUpdate(int val);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);

private:
    QOpenGLShaderProgram *program;
    QOpenGLShader* redShader;
    QOpenGLShader* blueShader;
    QOpenGLVertexArrayObject* VAO;
    QOpenGLBuffer* VBO;
    QOpenGLBuffer* EBO;

    QOpenGLVertexArrayObject* wVAO;
    QOpenGLBuffer* wVBO;
    QOpenGLBuffer* wEBO;

    GLuint posAttr;
    QMatrix4x4 projection;
    GLuint projectionLoc;
    QMatrix4x4 model;
    GLuint modelLoc;
    QMatrix4x4 view;
    GLuint viewLoc;

    bool notWireframe;

    int xRot;
    int yRot;
    int zRot;

    double heightAdj;

    GLuint waterLoc;
    GLuint waterAdjLoc;

    int waterEnabled;
    int water;
    float waterAdj;

    QPoint lastPos;

    dataHandler* data;
    dataHandler* wdata;

    int algo;
    int verts;
    int mySeed;

    QOpenGLTexture* tex;
    GLuint texAttr;
    int currentTex;

    double zoomAdj;
};

#endif // MYGLWIDGET_H

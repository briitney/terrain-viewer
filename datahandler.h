#ifndef DATAHANDLER_H
#define DATAHANDLER_H
#include <cstdlib>

class dataHandler
{
public:
    dataHandler(int w, int h, double** data);
    ~dataHandler();
    void dataToVerts();
    void indicesForGrid();
    double ** getData();
    double* getVerts();
    unsigned int* getIndices();
    int getw();
    int geth();
    int getVertsSize();
    int getIndicesSize();


private:
    int myW;
    int myH;
    double** myData;
    double* myVerts;
    unsigned int* myIndices;
    bool grabbed_data;
};

#endif // DATAHANDLER_H

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton_ToggleColor, SIGNAL(clicked()), ui->openGLWidget, SLOT(setColor()));
    connect(ui->pushButton_Water, SIGNAL(clicked()), ui->openGLWidget, SLOT(toggleWater()));
    connect(ui->pushButton_Exit, SIGNAL(clicked()), this, SLOT(exit()));
    connect(ui->horizontalSlider_Height, SIGNAL(valueChanged(int)), ui->openGLWidget, SLOT(setHeight(int)));

    ui->horizontalSlider_Height->setValue(50);
    connect(ui->horizontalSlider_WaterHeight, SIGNAL(valueChanged(int)), ui->openGLWidget, SLOT(setWaterHeight(int)));
    ui->horizontalSlider_WaterHeight->setValue(50);
    connect(ui->horizontalSlider_Zoom, SIGNAL(valueChanged(int)), ui->openGLWidget, SLOT(setZoom(int)));
    ui->horizontalSlider_Zoom->setValue(80);

    connect(ui->pushButton_Save, SIGNAL(clicked(bool)), ui->openGLWidget, SLOT(saveToImage()));
    connect(ui->pushButton_Load, SIGNAL(clicked(bool)), ui->openGLWidget, SLOT(loadFromImage()));
    connect(ui->pushButton_Refresh, SIGNAL(clicked(bool)), ui->openGLWidget, SLOT(refresh()));
    connect(ui->pushButton_Tex, SIGNAL(clicked(bool)), ui->openGLWidget, SLOT(changeTex()));

    ui->spinBox_Seed->setMaximum(RAND_MAX);
    connect(ui->openGLWidget, SIGNAL(seedUpdate(int)), ui->spinBox_Seed, SLOT(setValue(int)));
    connect(ui->pushButton_EnterSeed, SIGNAL(clicked()), this, SLOT(getAndSendSeedVal()));

    ui->spinBox_Vertices->setMaximum(500);
    ui->spinBox_Vertices->setSingleStep(10);
    ui->spinBox_Vertices->setMinimum(10);
    ui->spinBox_Vertices->setValue(100);
    connect(ui->spinBox_Vertices, SIGNAL(valueChanged(int)), ui->openGLWidget, SLOT(setVertNum(int)));

    QStringList items;
    items << tr("")
          << tr("Waffle")
          << tr("Random")
          << tr("Random with Fade")
          << tr("Gaussian")
          << tr("Perlin")
          << tr("Layered Perlin")
          << tr("Worley")
          << tr("Layered Worley");
    QStringListModel* typeModel = new QStringListModel(items, this);
    ui->comboBox_Algorithm->setModel(typeModel);
    connect(ui->comboBox_Algorithm, SIGNAL(currentIndexChanged(int)), ui->openGLWidget, SLOT(setAlgo(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::exit()
{
    QApplication::exit();
}

void MainWindow::getAndSendSeedVal()
{
    ui->openGLWidget->setSeed(ui->spinBox_Seed->value());
}
